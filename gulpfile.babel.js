import pkg from 'gulp';

const { task, src, dest, watch, series, parallel } = pkg;

import gulpSass from 'gulp-sass';
import * as darkSass from 'sass';
const sass = gulpSass(darkSass);
import fileinclude from 'gulp-file-include'
import browserSync from 'browser-sync';




task('styleCSS', () => {
    return src('./src/styles/*.scss')
        .pipe(sass.sync({
            outputStyle: 'expanded'
        })
        .on('error', sass.logError))
        .pipe(dest('./dist/css/'));
})

task ('moveHTML', () => {
    return src('./src/*.html')
        .pipe(fileinclude())
        .pipe(dest('./dist'));

})

task ('server', () => {
    return browserSync.init({
        server: {
            baseDir:['dist']
        },
        port:9000,
        open:true
    })
})


task('dev',series('styleCSS','moveHTML'));

task('watcher', () => {

    watch('./src/styles/**/*.scss', parallel('styleCSS')).on('change',browserSync.reload);
    watch('./src/**/*.html', parallel('moveHTML')).on('change',browserSync.reload);

})
task ('start',parallel('server','watcher'));